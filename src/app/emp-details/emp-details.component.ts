import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-emp-details',
  template: `<h2>
  Employee Details
  </h2>
  <ul *ngFor="let employee of employees">
  <li>{{employee.id}}.{{employee.name}} - {{employee.age}}</li>
  </ul>
  `,
  styleUrls: ['./emp-details.component.css']
})
export class EmpDetailsComponent implements OnInit {
  // public employees=[];
  public employees = [] as  any;

  constructor(private _employeeService:EmpService) { }

  ngOnInit()
  {
    this._employeeService.getEmployees()
    .subscribe(data=>this.employees=data);
  }

}
